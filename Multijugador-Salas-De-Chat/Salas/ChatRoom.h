#pragma once
#include "User.h"
#include <list>
#include <winsock2.h>
#include <string>
#pragma comment(lib,"ws2_32.lib") //Winsock Library

using namespace std;
class ChatRoom
{
public:
	ChatRoom();
	~ChatRoom();
	void Send();
	void SetMessage(char msg[]);
private:
	list <User> usersInRoom;
	char message[];
	bool priv;
};

