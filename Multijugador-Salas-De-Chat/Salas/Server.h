#pragma once
#include <list>
#include <winsock2.h>
#include "ChatRoom.h"

class Server
{
public:
	Server();
	~Server();
	void Init();
	void Listen();
private:
	list<ChatRoom> rooms; 
};

