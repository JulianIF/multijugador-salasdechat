#pragma once
#include<winsock2.h>

#pragma comment(lib,"ws2_32.lib") //Winsock Library

#define SERVER "127.0.0.1"  //ip address of udp server
#define BUFLEN 512  //Max length of buffer
#define PORT 8888   //The port on which to listen for incoming data
#include <string>

class User
{
public:
	User();
	~User();
	void CrearSala(string nombreSala);
	void CrearSalaPriv(string nombreSala, User u);
	void EntrarASala(int numSala);
	void RecibirMensaje();
	void EnviarMensaje();
	
private:
	string nickName;
	int room;
	struct sockaddr_in si_other;
	int s, slen = sizeof(si_other);
	char buf[BUFLEN];
	char message[BUFLEN];
	WSADATA wsa;
	void EscribirMensaje();
	string server;
};

